<?php

namespace FluxTech\CiceronSDK;

use FluxTech\CiceronSDK\doc\ClientDoc;
use SoapClient;
use Exception;

class Client extends ClientDoc
{
    /**
     * @var SoapClient
     */
    private $soapClient;

    /**
     * @var string
     */
    private $wsdl;

    public function __construct($wsdl = 'https://edemo.siriusit.net/CiceronWS/diabasplugin/DiabasService/wsdl', $dev = true)
    {
        $this->wsdl = $wsdl;
        if ($dev) {
            ini_set('soap.wsdl_cache_enabled',0);
            ini_set('soap.wsdl_cache_ttl',0);
        }
        $this->soapClient = new SoapClient($this->wsdl, [
            'exceptions' => true,
            'trace' => true
        ]);
    }

    public function __call($name, $arguments)
    {
        try {
            return $this->soapClient->__soapCall($name, [$arguments[0]], [
                'location' => sprintf('%s/%s', $this->wsdl, $name)
            ]);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function getSupportedMethods()
    {
        return $this->soapClient->__getFunctions();
    }
}