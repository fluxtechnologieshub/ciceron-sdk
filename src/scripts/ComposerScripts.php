<?php

namespace FluxTech\CiceronSDK\scripts;

use SoapClient;

class ComposerScripts
{
    public static function regenerateClassMap()
    {
        $url = 'https://edemo.siriusit.net/CiceronWS/diabasplugin/DiabasService/wsdl';
        $client = new SoapClient($url);
        $classes = [];
        foreach ($client->__getTypes() as $type) {
            $lines = explode("\n", $type);
            array_pop($lines);
            $rows = array_map(function ($row) {
                return explode(' ', trim($row, ' }{;'));
            }, $lines);
            $className = array_shift($rows);

            $classes[$className[1]] = array_map(function ($row) {
                return [
                    'type' => $row[0],
                    'name' => sprintf('$%s', $row[1])
                ];
            }, $rows);
        }
        $classTempate = <<<PHP
<?php

namespace FluxTech\CiceronSDK\Struct;

use FluxTech\CiceronSDK\StructAbstract;

/**
 * Class {className}
 * @package FluxTech\CiceronSDK\Struct
 * @author Flux Technologies
{properties}
 */
class {className} extends StructAbstract
{
{vars}
}

PHP;

        foreach ($classes as $className => $properties) {
            if (empty($className)) {
                continue;
            }
            $propertyDocs = [
                ' *'
            ];
            $vars = [];
            foreach ($properties as $property) {
                $propertyDocs[] = sprintf(' * @property %s %s', $property['type'], $property['name']);
                $vars[] = sprintf("\tpublic %s;", $property['name']);
            }
            $content = $classTempate;
            $content = str_replace('{className}', $className, $content);
            $content = str_replace('{properties}', implode(PHP_EOL, $propertyDocs), $content);
            $content = str_replace('{vars}', implode(PHP_EOL, $vars), $content);
            file_put_contents(sprintf('%s/../Struct/%s.php', __DIR__, $className), $content);
        }

    }

    public static function regenerateClientDocs()
    {
        $url = 'https://edemo.siriusit.net/CiceronWS/diabasplugin/DiabasService/wsdl';
        $client = new SoapClient($url);

        $classTempate = <<<PHP
<?php

namespace FluxTech\CiceronSDK\doc;

/**
 * Class ClientDoc
 * @package FluxTech\CiceronSDK\doc
 * @author Flux Technologies
 *
{properties} 
 */
class ClientDoc
{

}
PHP;

        $properties = [];
        foreach ($client->__getFunctions() as $method) {
            $method = str_replace('(', '(\FluxTech\CiceronSDK\Struct\\', $method);
            $properties[] = " * @method \FluxTech\CiceronSDK\Struct\\$method<br/>";
        }
        $content = $classTempate;
        $content = str_replace('{properties}', implode(PHP_EOL, $properties), $content);
        file_put_contents(sprintf('%s/../doc/ClientDoc.php', __DIR__), $content);
    }
}