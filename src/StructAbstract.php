<?php

namespace FluxTech\CiceronSDK;

class StructAbstract
{
    public function __construct(array $fields = [])
    {
        $this->setAttributes($fields);
    }

    public function setAttributes(array $fields)
    {
        foreach ($fields as $key => $value) {
            $this->{$key} = $value;
        }
    }
}